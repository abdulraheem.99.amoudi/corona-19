<?php

use Illuminate\Support\Facades\Route;
use App\Classes\Covid;
use Illuminate\Support\Facades\Http;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $countries = Covid::getSummary();
    return view('index', compact('countries'));
})->name('home');

Route::get('/symptoms', function () {
    return view('covid-19.symptoms');
})->name('symptoms');

Route::get('/treatments', function () {
    return view('covid-19.treatments');
})->name('treatments');

Route::get('/bibp', function () {
    return view('covid-19.vaccines.bibp');
})->name('bibp');

Route::get('/astrazeneca', function () {
    return view('covid-19.vaccines.astrazeneca');
})->name('astrazeneca');

Route::get('/sputnik', function () {
    return view('covid-19.vaccines.sputnik');
})->name('sputnik');

Route::get('/Pfizer', function () {
    return view('covid-19.vaccines.Pfizer');
})->name('Pfizer');

Route::get('/infection', function () {
    return view('covid-19.infection');
})->name('infection');

Route::get('/protection', function () {
    return view('covid-19.protection');
})->name('protection');

Route::get('/sick', function () {
    return view('covid-19.sick');
})->name('sick');

//Route::get('/countries', [CovidController::class, 'getCountries']);
Route::get('countries', 'App\Http\Controllers\CovidController@getCountries')->name('countries');