<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\Covid;

class CovidController extends Controller
{
    
    public function getCountries(Request $request) {
        $countries = Covid::getSummary();
       return view('covid-19.overview.countries', compact('countries'));
    }
}