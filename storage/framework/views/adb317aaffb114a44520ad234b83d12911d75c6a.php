
<?php $__env->startSection('title'); ?>
Covid-19 Countries
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<!--
<?php $__currentLoopData = $countries['Countries']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
<li><i class="fa fa-star"></i> <?php echo e(print_r($country['Country'])); ?></li>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
-->
<!-- /.card -->
<!-- general form elements disabled -->
<div class="card card-secondary" style="margin-top:20px">
    <div class="card-header">
        <h3 class="card-title">Countries</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table id="example2" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>Country</th>
                    <th>NewConfirmed</th>
                    <th>TotalConfirmed</th>
                    <th>NewDeaths</th>
                    <th>TotalDeaths</th>
                    <th>NewRecovered</th>
                    <th>TotalRecovered</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody>
                <?php $__currentLoopData = $countries['Countries']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td> <?php echo $country['Country'] ?></td>
                    <td> <?php echo $country['NewConfirmed'] ?></td>
                    <td> <?php echo $country['TotalConfirmed'] ?></td>
                    <td> <?php echo $country['NewDeaths'] ?></td>
                    <td> <?php echo $country['TotalDeaths'] ?></td>
                    <td> <?php echo $country['NewRecovered'] ?></td>
                    <td> <?php echo $country['TotalRecovered'] ?></td>
                    <td> <?php echo $country['Date'] ?></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </tbody>
            <tfoot>
                <tr>
                    <th>Country</th>
                    <th>NewConfirmed</th>
                    <th>TotalConfirmed</th>
                    <th>NewDeaths</th>
                    <th>TotalDeaths</th>
                    <th>NewRecovered</th>
                    <th>TotalRecovered</th>
                    <th>Date</th>
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cssstyle'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo e(asset('covid-19/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('covid-19/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('covid-19/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')); ?>">
<!-- Theme style -->
<link rel="stylesheet" href="<?php echo e(asset('covid-19/dist/css/adminlte.min.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('jscode'); ?>
<!-- DataTables  & Plugins -->
<script src="<?php echo e(asset('covid-19/plugins/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(asset('covid-19/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')); ?>"></script>
<script src="<?php echo e(asset('covid-19/plugins/datatables-responsive/js/dataTables.responsive.min.js')); ?>"></script>
<script src="<?php echo e(asset('covid-19/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')); ?>"></script>
<script src=".<?php echo e(asset('covid-19/plugins/datatables-buttons/js/dataTables.buttons.min.js')); ?>"></script>
<script src="<?php echo e(asset('covid-19/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')); ?>"></script>
<script src="<?php echo e(asset('covid-19/plugins/jszip/jszip.min.js')); ?>"></script>
<script src="<?php echo e(asset('covid-19/plugins/pdfmake/pdfmake.min.js')); ?>"></script>
<script src="<?php echo e(asset('covid-19/plugins/pdfmake/vfs_fonts.js')); ?>"></script>
<script src="<?php echo e(asset('covid-19/plugins/datatables-buttons/js/buttons.html5.min.js')); ?>"></script>
<script src="<?php echo e(asset('covid-19/plugins/datatables-buttons/js/buttons.print.min.js')); ?>"></script>
<script src="<?php echo e(asset('covid-19/plugins/datatables-buttons/js/buttons.colVis.min.js')); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo e(asset('covid-19/dist/js/adminlte.min.js')); ?>"></script>

<script>
$(function() {

    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\Corona-19\resources\views/covid-19/overview/countries.blade.php ENDPATH**/ ?>