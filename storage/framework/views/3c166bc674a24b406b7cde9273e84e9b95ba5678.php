
<?php $__env->startSection('title'); ?>
Covid-19 Symptoms
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<!-- /.card -->
<!-- general form elements disabled -->
<div class="card card-secondary" style="margin-top:20px">
    <div class="card-header">
        <h3 class="card-title">Symptoms</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <p>COVID-19 affects different people in different ways. Most people who develop it have mild to moderate
            symptoms and recover without hospitalization.
            <br> <b>The most common symptoms:</b>
        <ul>
            <li>Fever</li>
            <li>dry cough</li>
            <li>Exhaustion</li>
            <li>Less common symptoms</li>
            <li>Aches and pains</li>
            <li>Sore throat</li>
            <li>Diarrhea</li>
            <li>conjunctivitis</li>
            <li>a headache</li>
            <li>Loss of sense of taste or smell</li>
            <li>Skin rash, or change in the color of the fingers or toes</li>
        </ul>

        <b>Serious symptoms</b>
        <ul>
            <li>Difficulty or shortness of breath</li>
            <li>chest pain or pressure</li>
            <li>Loss of ability to speak or move</li>
        </ul>


        Seek immediate medical attention if you have serious symptoms. Always contact your doctor or health facility
        before going.
        People with mild symptoms should treat their symptoms at home, even if they are otherwise healthy.
        It takes an average of 5-6 days for symptoms to appear since a person is infected with the virus, but it can
        take up to 14 days for symptoms to appear.</p>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\Corona-19\resources\views/covid-19/symptoms.blade.php ENDPATH**/ ?>