
<?php $__env->startSection('title'); ?>
Covid-19 Sputnik
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<!-- /.card -->
<!-- general form elements disabled -->
<div class="card card-success" style="margin-top:20px">
    <div class="card-header">
        <h3 class="card-title">Sputnik</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <p>
            Sputnik V is the world's first registered vaccine based on the well-studied human adenovirus platform. The
            vaccine "Sputnik V" has been registered in more than 65 countries. <br>
            More than 31,000 volunteers are participating in the clinical study that is taking place after the
            registration of the "Sputnik V" vaccine in Russia. The third phase of clinical studies of the "Sputnik V"
            vaccine is underway in the United Arab Emirates, India, Venezuela and Belarus. <br>
            "Sputnik V" is one of three vaccines in the world with an efficacy of more than 90%. The effectiveness of
            the vaccine was calculated at the level of 91.6% based on the data of 19,866 volunteers who received the
            first and second injections of the “Sputnik V” vaccine or the placebo, and 78 confirmed cases of Covid-19
            infection were recorded during the last control and monitoring phase. <br>
            As a result of analyzing the data of 3.8 million vaccinated Russian citizens, the "Sputnik V" vaccine showed
            an efficacy of 97.6%. <br>
            The price of a single dose of "Sputnik V" vaccine for foreign markets will be less than 10 dollars ("Sputnik
            V" is a two-dose vaccine). <br>
            The vaccine can be stored in its lyophilized form at between +2 and +8 °C, allowing it to be easily
            distributed throughout the world, including in hard-to-reach areas. <br>
            The international partners of the Russian Fund for Direct Investments in India, Brazil, China, South Korea
            and other countries will produce the vaccine for supply to the global market. This site was created in order
            to provide new and accurate information about the "Sputnik V" vaccine. <br> <br>
            <b>Certificate of registration granted by the Russian Ministry of Health</b> <br>
            The vaccine is named after the first Soviet satellite. The launch of "Sputnik-1" in 1957 gave a new impetus
            to space research around the world, and constituted the so-called "Sputnik moment" for the global community.
            The Ministry of Health of the Russian Federation, the National Aesthetic Research Center for Epidemiology
            and Microbiology of the Ministry of Health of the Russian Federation and the Russian Direct Investment Fund
            (the Sovereign Wealth Fund of the Russian Federation, RDIF) announced the registration of a single-component
            vaccine “Sputnik Light” against infection with the novel coronavirus. <br>
            Sputnik Light” is the first component (serotype 26 of human adenovirus (Ad26)) of the “Sputnik V” vaccine,
            the world’s first registered vaccine against MERS-CoV. <br>
            The effectiveness of the single component vaccine "Sputnik Light" was 79.4% based on analysis of data from
            the 28th day after citizens of the Russian Federation were immunized as part of the mass vaccination program
            from December 5, 2020 to April 15, 2021. <br>
            Its effectiveness rate is about 80%, which exceeds that of many vaccines that require two injections.
            Sputnik Light” is effective against all strains of the novel coronavirus according to the results of
            laboratory studies of the National Aesthetic Research Center for Epidemiology and Microbiology. <br>
            The Phase I/II Safety and Immunology Study of the Sputnik Light Vaccine demonstrated the following:
            A single dose of Sputnik Light can provoke the development of specific IgG antibodies in 96.9% of
            individuals 28 days after vaccination. <br>
            Virus neutralizing antibodies are produced in 91.67% of individuals on the 28th day after vaccination with
            the Sputnik Light vaccine. <br>
            The cellular immune response to the SARS-CoV-2 protein S is formed in 100% of vaccines already vaccinated by
            the 10th day. <br>
            Immunization with the Sputnik Light vaccine for people with previous immunity to SARS-CoV-2, allows a more
            than 40-fold increase in the level of antigen-specific IgG antibodies in 100% of those vaccinated on the
            10th day; <br>
            Immunization of individuals with pre-existing immunity to SARS-Cov2 with Sputnik Light can increase the
            level of antigen-specific IgG antibodies in 100% of subjects 10 days after immunization; No serious adverse
            events have been reported after vaccination with Sputnik Light. <br>
            The two-dose Sputnik V vaccine remains the main vaccination tool.
        </p>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\Corona-19\resources\views/covid-19/vaccines/sputnik.blade.php ENDPATH**/ ?>