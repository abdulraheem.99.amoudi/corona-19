
<?php $__env->startSection('title'); ?>
Covid-19 BIBP
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="col-12" id="accordion" style="margin-top:20px">
    <div class="card card-primary card-outline">
        <a class="d-block w-100" data-toggle="collapse" href="#collapseOne">
            <div class="card-header">
                <h4 class="card-title w-100">
                    1. What categories should be given priority for vaccination?
                </h4>
            </div>
        </a>
        <div id="collapseOne" class="collapse " data-parent="#accordion">
            <div class="card-body">
                With the limited supply of COVID-19 vaccines, priority should be given to vaccination for health workers
                at particular risk of infection and the elderly. <br>
                The vaccine is not recommended for people under 18 years of age before the results of further studies in
                that age group are released. <br>
                Countries can refer to the Strategic Advisory Group of Experts on Immunization Roadmap to Prioritize
                Uses of COVID-19 Vaccines in the Context of Limited Supplies and the Strategic Advisory Group of Experts
                on Immunization Values ​​Framework on the Distribution and Prioritization of COVID-19 Vaccines to guide
                their prioritization of target groups. <br>
            </div>
        </div>
    </div>
    <div class="card card-primary card-outline">
        <a class="d-block w-100" data-toggle="collapse" href="#collapseTwo">
            <div class="card-header">
                <h4 class="card-title w-100">
                    2. Should pregnant women be vaccinated?
                </h4>
            </div>
        </a>
        <div id="collapseTwo" class="collapse" data-parent="#accordion">
            <div class="card-body">
                Available data on the immunization of pregnant women with the BIBP vaccine against COVID-19 are
                insufficient to assess the efficacy of the vaccine or the risks associated with this vaccine during
                pregnancy. After all, this is an inactivated adjuvant vaccine that is routinely used in many other
                vaccines and has documented good safety, including in pregnant women. The efficacy of the BIBP vaccine
                against COVID-19 in pregnant women is therefore expected to be similar to that observed in non-pregnant
                women of similar age. <br>
                Meanwhile, WHO recommends the use of the BIBP vaccine against COVID-19 in pregnant women when the
                benefits of vaccinating the pregnant woman outweigh the potential risks. To help pregnant women make
                this assessment, they should be provided with information about the risks of COVID-19 during pregnancy;
                the potential benefits of vaccination in the local epidemiological context; and limitations of safety
                data for currently pregnant women. WHO does not recommend taking a pregnancy test before vaccination. It
                also does not recommend delaying or considering termination of pregnancy due to vaccination.
            </div>
        </div>
    </div>
    <div class="card card-primary card-outline">
        <a class="d-block w-100" data-toggle="collapse" href="#collapseThree">
            <div class="card-header">
                <h4 class="card-title w-100">
                    3. What other groups can receive the vaccination?
                </h4>
            </div>
        </a>
        <div id="collapseThree" class="collapse" data-parent="#accordion">
            <div class="card-body">
                The vaccine can be given to people who have previously had COVID-19. Available data show that, within 6
                months after initial normal infection, re-infection with symptoms is uncommon. Given the limited supply
                of vaccines, people who have PCR-confirmed SARS-2 virus infection in the previous six months may
                therefore choose to delay vaccination until near the end of this period. In environments where mutated
                strains are circulating raising concerns and with evidence of immune withdrawal, early post-infection
                immunization may be desirable. <br>
                The efficacy of the vaccine is expected to be similar in lactating women and other adults. WHO
                recommends the use of the BIBP vaccine against COVID-19 in nursing mothers, as in all adults. It is also
                not recommended to stop breastfeeding after receiving the vaccination. <br>
                People living with HIV are at greater risk of developing severe complications from COVID-19. People
                infected with this virus were not included in the trial but, given that this is a non-clonal vaccine,
                people with the virus who fall into the recommended group can be vaccinated. Information and advice
                should be provided, where possible, to guide the assessment of benefits versus individual risks. <br>
            </div>
        </div>
    </div>
    <div class="card card-warning card-outline">
        <a class="d-block w-100" data-toggle="collapse" href="#collapseFour">
            <div class="card-header">
                <h4 class="card-title w-100">
                    4. What groups are not recommended for vaccination?
                </h4>
            </div>
        </a>
        <div id="collapseFour" class="collapse" data-parent="#accordion">
            <div class="card-body">
                The vaccine should not be given to individuals who have previously been hypersensitive to any component
                of the vaccine. <br>
                Anyone with a body temperature of more than 38.5 degrees Celsius should postpone the vaccination until
                the fever is gone.
            </div>
        </div>
    </div>
    <div class="card card-warning card-outline">
        <a class="d-block w-100" data-toggle="collapse" href="#collapseFive">
            <div class="card-header">
                <h4 class="card-title w-100">
                    5. What is the recommended dose?
                </h4>
            </div>
        </a>
        <div id="collapseFive" class="collapse" data-parent="#accordion">
            <div class="card-body">
                The SAGE recommends the use of the BIBP vaccine as two doses (0.5 mL) administered intramuscularly. WHO
                recommends an interval of 3 to 4 weeks between the first and second doses. If the second dose is given
                less than 3 weeks after the first, the dose does not need to be repeated. If the second dose is delayed
                beyond 4 weeks, it should be given as soon as possible. It is recommended that all vaccinated
                individuals receive two doses.
            </div>
        </div>
    </div>
    <div class="card card-warning card-outline">
        <a class="d-block w-100" data-toggle="collapse" href="#collapseSix">
            <div class="card-header">
                <h4 class="card-title w-100">
                    6. How does this vaccine compare to other vaccines already in use?
                </h4>
            </div>
        </a>
        <div id="collapseSix" class="collapse" data-parent="#accordion">
            <div class="card-body">
                We cannot compare vaccines directly due to different approaches to the design of the studies involved,
                and in general, all vaccines that fulfilled the WHO emergency use procedure under the Emergency Use
                Protocol are highly effective in preventing severe complications of COVID-19 and avoiding the need for
                admission hospitals for treatment.
            </div>
        </div>
    </div>
    <div class="card card-danger card-outline">
        <a class="d-block w-100" data-toggle="collapse" href="#collapseSeven">
            <div class="card-header">
                <h4 class="card-title w-100">
                    7. Is the vaccine safe?
                </h4>
            </div>
        </a>
        <div id="collapseSeven" class="collapse" data-parent="#accordion">
            <div class="card-body">
                The Strategic Advisory Group of Experts on Immunization (SAGE) has carefully evaluated the data on the
                quality, safety and efficacy of the vaccine and recommended its use in persons aged 18 years or over.
                <br>
                However, safety data are limited for people over 60 years of age (due to the small number of
                participants in clinical trials). While it is not possible to expect differences in the safety profile
                of the vaccine in the elderly compared with the younger age groups, countries considering the use of
                this vaccine in persons over 60 years of age should continue to actively monitor its safety.
            </div>
        </div>
    </div>
    <div class="card card-danger card-outline">
        <a class="d-block w-100" data-toggle="collapse" href="#collapseEight">
            <div class="card-header">
                <h4 class="card-title w-100">
                    8. How effective is the vaccine?
                </h4>
            </div>
        </a>
        <div id="collapseEight" class="collapse" data-parent="#accordion">
            <div class="card-body">
                A large-scale, phase 3, multi-country trial showed that two doses, 21 days apart, have an efficacy of
                79% against symptomatic SARS-CoV-2 infection 14 days or more after the second dose. The effectiveness of
                the vaccine in avoiding the need for hospitalization was 79%. <br>
                However, the trial was not designed and conducted to demonstrate the efficacy of the vaccine against
                severe complications of COVID-19 in people with co-morbidities, during pregnancy, or in people 60 years
                of age and older. Women were underrepresented in the experiment. The average follow-up time available at
                the time of the evidence review was 112 days. <br>
                Two other trials of effectiveness are currently underway, but these data are not yet available.
            </div>
        </div>
    </div>
    <div class="card card-danger card-outline">
        <a class="d-block w-100" data-toggle="collapse" href="#collapseNine">
            <div class="card-header">
                <h4 class="card-title w-100">
                    9. Does the vaccine work against the new mutated strains of the SARS-CoV-2 virus?
                </h4>
            </div>
        </a>
        <div id="collapseNine" class="collapse" data-parent="#accordion">
            <div class="card-body">
                As required by the Strategic Advisory Group of Experts on Immunization (SAGE) roadmap to prioritize uses
                of COVID-19 vaccines in the context of limited supplies, the Group is currently recommending the use of
                this vaccine. <br>
                As new data becomes available, WHO will update these recommendations accordingly. This vaccine has not
                yet been evaluated in the context of the spread of mutated strains of widespread concern.
            </div>
        </div>
    </div>
    <div class="card card-danger card-outline">
        <a class="d-block w-100" data-toggle="collapse" href="#collapseTen">
            <div class="card-header">
                <h4 class="card-title w-100">
                    10. Does the vaccine protect against infection and its transmission to others?
                </h4>
            </div>
        </a>
        <div id="collapseTen" class="collapse" data-parent="#accordion">
            <div class="card-body">
                There are currently no objective data available regarding the effect of the BIBP vaccine against
                COVID-19 on the transmission of the SARS-CoV-2 virus that causes COVID-19. <br>
                At the same time, WHO reminds the need to continue to apply and strengthen effective public health
                measures: wearing masks, physical distancing, washing hands, observing respiratory and cough etiquette,
                avoiding crowds and ensuring good ventilation.
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\Corona-19\resources\views/covid-19/vaccines/bibp.blade.php ENDPATH**/ ?>