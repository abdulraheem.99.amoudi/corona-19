
<?php $__env->startSection('title'); ?>
Covid-19 Infection
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<!-- /.card -->
<!-- general form elements disabled -->
<div class="card card-primary" style="margin-top:20px">
    <div class="card-header">
        <h3 class="card-title">Ways of transmission of the virus that causes Covid-19 disease: -</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <p>
            Respiratory diseases can be transmitted by droplets of different sizes as follows: If the diameter of the
            particles exceeds 5-10 micrometers, these particles are referred to as respiratory droplets. If their
            diameter is 5 micrometers or less, they are referred to as droplet nuclei.1 According to the current
            available evidence, infection with the virus that causes Covid-19 disease is transmitted mainly from one
            person to another through respiratory and contact droplets.2-7 In an analysis of a total of 75,465 A case of
            COVID-19 in China, no airborne transmission has been reported.8<br>
            The infection is transmitted through droplets when a person comes into close contact (within a distance of
            one meter) with another person who exhibits respiratory symptoms (such as coughing or sneezing), which makes
            this person at risk of exposure to respiratory droplets in the mucous membranes (mouth, nose) or conjunctiva
            (eyes). to be contagious. Infection may also be transmitted through contaminated tools found in the
            immediate environment surrounding the infected person.8 Therefore, infection with the virus that causes
            Covid-19 disease can be transmitted either through direct contact with infected persons or indirect contact
            with surfaces in the immediate surrounding environment. or instruments used on an infected person (such as a
            stethoscope or thermometer).<br>
            Air transmission differs from droplet transmission because air transmission refers to the presence of
            microbes inside the nuclei of droplets, which are generally considered particles with a diameter of 5
            micrometers or less and can remain in the air for long periods of time and transmit from one person to
            another at distances of more than one meter.<br>
            In the context of COVID-19, aerobic transmission may be possible in certain circumstances and contexts in
            which supportive aerosol-generating procedures or therapies are applied, i.e. endotracheal intubation,
            bronchoscopy, open suction, aerosol therapy, manual ventilation prior to intubation, prone positioning, and
            disconnection from breathing Artificial, non-invasive positive pressure ventilation, tracheostomy, and
            cardiopulmonary resuscitation.<br>
            There is some evidence that infection with COVID-19 may lead to intestinal infection and be present in the
            faeces. However, to date, there is only one study in which the virus that causes COVID-19 was cultured from
            a single stool sample.9 Fecal-oral transmission of this virus has not yet been reported.<br><br>
            <b>Implications of recent findings regarding detection of the virus that causes COVID-19 in air
                samples</b><br>
            So far, some scientific publications report that there is preliminary evidence that the virus that causes
            COVID-19 can be detected in the air, and therefore some media organizations have indicated that airborne
            transmission has occurred. Caution is required in interpreting these preliminary results.<br>
            In a recent article published in the New England Journal of Medicine, the sustainability of the virus that
            causes COVID-19 was assessed. 10 In this pilot study, the atomizer was generated using a triple jet colizone
            atomizer and introduced into a Goldberg cylinder under controlled laboratory conditions. And this is a
            high-powered machine that does not capture the normal conditions of a human cough. In addition, the presence
            of the virus that causes COVID-19 in aerosol particles over a period of three hours does not reflect the
            clinical context in which aerosol generating procedures are used. Therefore, it is a matter of aerosol
            generating procedure in an experimental setting.<br>
            There is information reported by facilities hosting asymptomatic COVID-19 patients but the presence of RNA
            of the virus that causes COVID-19 in air samples was not detected. 11-12 WHO is aware of other studies that
            evaluated the presence of virus RNA in samples. air but has not yet been published in specialized journals.
            It should be noted that detection of viral RNA in environmental samples based on PCR assays does not mean
            that the virus is viable and transmissible. Further studies are necessary to determine the potential for
            detection of the virus that causes COVID-19 in air samples from patient rooms where no supportive
            aerosol-generating procedures or therapies are applied. As new evidence emerges, it is important to
            determine whether the virus detected is viable and what its possible role in transmission is.<br><br>
            <b>Conclusions</b><br>
            Based on the available evidence, including the recent publications mentioned above, WHO continues to
            recommend that persons involved in the care of patients with COVID-19 should take droplet precautions and
            avoid contact. WHO also continues to recommend precautionary measures to avoid airborne transmission in
            settings and contexts in which supportive aerosol-generating procedures and treatments are applied, as
            assessed by risk.13 These recommendations are consistent with other national and international guidelines,
            including those issued by the European Society of Intensive Care Medicine and Critical care medicine14 and
            guidelines currently in use in Australia, Canada, and the United Kingdom.<br>
            At the same time, other countries and organizations, including the US Centers for Disease Control and
            Prevention and the European Center for Disease Prevention and Control, recommend that precautions be taken
            to avoid airborne transmission in any setting involving care for people with Covid-19 disease, and the use
            of medical masks is an acceptable option. In case of shortage of respirator masks N95, FFP2 or FFP3.  <br>
            The current recommendations issued by the organization stress the importance of rational and appropriate use
            of all personal protective equipment20 and not only the use of masks, which requires health care workers to
            adhere to strict proper behavior, especially with regard to procedures for removing personal protective
            equipment and hand hygiene rules.21 The organization also trains staff on these recommendations22 and
            procures and makes adequate provision for personal protective equipment and other necessary supplies and
            equipment. Finally, the organization still stresses the paramount importance attached to washing hands
            regularly, adhering to the due etiquette when coughing or sneezing, cleaning and sterilizing the
            environment, and the importance of maintaining a safe distance from others and avoiding close contact with
            people who have a fever or show respiratory symptoms, without taking appropriate protection measures.<br>
            WHO is carefully monitoring emerging evidence on this critical topic and will update this scientific brief
            as more information becomes available.
        </p>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\Corona-19\resources\views/covid-19/infection.blade.php ENDPATH**/ ?>