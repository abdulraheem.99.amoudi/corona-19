@extends('layouts.master')
@section('title')
Covid-19 Treatments
@endsection
@section('content')
<!-- /.card -->
<!-- general form elements disabled -->
<div class="card card-success" style="margin-top:20px">
    <div class="card-header">
        <h3 class="card-title">Treatments</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <p>
            <b>self care</b><br>
            After exposure to someone with COVID-19, do the following:
        <ul>
            <li>Call your healthcare provider or the COVID-19 hotline to find out where and when to get tested.</li>
            <li>Cooperate with contact tracing measures to stop the spread of the virus.</li>
            <li>If the test is not available, stay home and stay away from others for 14 days.</li>
            <li>While you are in quarantine, do not go to work, school or public places.</li>
            <li>Ask someone to bring you supplies.</li>
            <li>Keep at least 1 meter away from others, even your family members.</li>
            <li>Wear a medical mask to protect others, including if/when you need to seek medical attention.</li>
            <li>Clean your hands frequently.</li>
            <li>Stay in a separate room from the rest of the family, and if this is not possible, wear a mask.</li>
            <li>Keep the room well ventilated.</li>
            <li>If you share a room, keep the beds at least 1 meter apart.</li>
            <li>Monitor yourself for any symptoms for 14 days.</li>
        </ul>

        <b>Call your health care provider right away if you have any of the following danger signs:</b><br>
        <ul>
            <li>Difficulty breathing, loss of speech or movement, confusion of mind, or chest pain.</li>
            <li>Maintain a positive outlook by communicating with loved ones over the phone or the Internet, as well as
                exercising at home.</li>
        </ul>
        </p>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
@endsection