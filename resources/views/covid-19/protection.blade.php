@extends('layouts.master')
@section('title')
Covid-19 Protection
@endsection
@section('content')
<!-- /.card -->
<!-- general form elements disabled -->
<div class="card card-info" style="margin-top:20px">
    <div class="card-header">
        <h3 class="card-title">Corona virus prevention measures:-</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <p>
            <b>What to do to protect yourself and others from COVID-19</b>
        <ul>
            <li>Keep a distance of at least one meter from others to reduce the risk of infection when they cough,
                sneeze
                or talk. Keep an even greater distance from others when indoors. The greater the distance, the better.
            </li>
            <li>Make it a habit to wear a mask when you are with other people. Proper use, preservation, cleaning and
                disposal of masks is essential to make them as effective as possible.</li>
        </ul>
        </p>
        <br>

        <p>
            <b>Here is the basic information on how to wear a mask:</b>
        <ul>
            <li>Clean your hands before putting on the mask, and before and after taking it off.</li>
            <li>Make sure it covers your nose, mouth and chin.</li>
            <li>When you remove the mask, keep it in a clean plastic bag, and be sure to wash it daily if it is a cloth
                mask, or dispose of it in the waste bin if it is a medical mask.</li>
            <li> Do not use masks with valves.</li>
        </ul>
        </p>

        <p>
            <b>How to make your environment safer</b>
        <ul>

            <li><b>Avoid the three memes: enclosed, crowded or close contact.</b>
                <ul>
                    <li>Outbreaks are reported in restaurants, choir rehearsals, fitness classes, nightclubs, offices,
                        places of
                        worship where people gather, and often in crowded enclosed spaces where people are talking
                        loudly, shouting,
                        breathing heavily or singing
                    </li>
                    <li>The risk of contracting COVID-19 increases in crowded, poorly ventilated places where infected
                        people
                        spend long periods of time together in close proximity to each other. It is these environments
                        in which the
                        virus appears to spread more efficiently via respiratory droplets or aerosols, so precautions
                        are more
                        important.</li>
                </ul>
            </li>
            <li><b>Meet people abroad. Outdoor gatherings are safer than indoor gatherings, especially if the indoor
                    spaces
                    are small and do not get outside air.</b>
            </li>
            <li><b>
                    Avoid crowded or closed places, but if you can't, take the following precautions:
                </b>
                <ul>
                    <li>
                        Open a window. Increase the volume of Natural Ventilation when indoors.
                    </li>
                    <li>
                        The organization has published questions and answers on ventilation and air conditioning for
                        both the
                        general public and people managing public spaces and buildings.
                    </li>
                    <li>
                        Wear a mask .
                    </li>
                </ul>
            </li>
        </ul>
        </p>

        <p>
            <b>Don't forget the basics of good hygiene</b><br>
        <ul>
            <li>
                <b>Regularly clean your hands with alcohol-based hand sanitizer or wash them with soap and water.</b>
                This
                removes germs, including viruses, that may be on your hands.
            </li>
            <li>
                <b>Avoid touching your eyes, nose and mouth.</b> Hands touch many surfaces and can pick up viruses. If
                hands
                become contaminated, the virus can be transmitted to the eyes, nose or mouth. The virus can enter your
                body
                through these ports and cause you to become ill.
            </li>
            <li>
                <b>Cover your mouth and nose with your elbow or a tissue when coughing or sneezing.</b> Then dispose of
                the
                tissue immediately in a closed trash can. By following good respiratory hygiene practices, you protect
                the
                people around you from viruses such as the viruses that cause colds, influenza and COVID-19.
            </li>
            <li>
                <b><b>Clean and disinfect surfaces frequently, especially those that are touched regularly,</b> such as
                    door
                    handles, faucets, and phone screens.
            </li>
        </ul>
        </p>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
@endsection