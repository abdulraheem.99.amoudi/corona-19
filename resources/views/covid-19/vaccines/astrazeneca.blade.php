@extends('layouts.master')
@section('title')
Covid-19 Oxford-AstraZeneca
@endsection
@section('content')
<!-- /.card -->
<!-- general form elements disabled -->
<div class="card card-success" style="margin-top:20px">
    <div class="card-header">
        <h3 class="card-title">Oxford-AstraZeneca</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <p>
            <b>The Oxford-AstraZeneca vaccine or AZD1222</b> (19, referred to in the media as: AstraZeneca vaccine,
            Oxford
            vaccine, or recently Vaccinaria vaccine is a vaccine against coronavirus disease, which the University of
            Oxford worked with the British-Swedish company AstraZeneca, to develop and produce, and is intended for
            administration By intramuscular injection, vaccine research is being conducted by a team consisting of the
            Edward Jenner Institute for Vaccine Research and the Oxford Vaccine Group, led by Sarah Gilbert, Adrian
            Hill, Andrew Pollard, Theresa Lampe, Sandy Douglas and Catherine Green. Phase III clinical trial.
            The efficacy of the vaccine was 90% when following the dosing regimen, which includes giving half the dose,
            followed by a full dose after at least one month. This is the result of various experiments that included
            participants, all of whom were younger than 55 years. Another dosing regimen showed an efficacy of 62% when
            two complete doses were given at least 1 month apart. <br>
            The research was conducted by the Edward Jenner Institute and the Vaccine Research Group at the University
            of Oxford in collaboration with Italian vaccine manufacturer Advent SRI in Pomezia on the IRPM campus. This
            factory produced the first batch of a COVID-19 vaccine for use in clinical trials. The research team was led
            by Sarah Gilbert, Adrian Hill, Andrew Pollard, Theresa Lambie, Sandy Douglas and Kathryn Green. <br>
            The vaccine was approved for inclusion in the UK's vaccination program on 30 December 2020, and the first
            dose was given on 4 January 2021. <br>
            In March 2021, some countries including Germany, France, Italy, Spain, the Netherlands, Norway, Denmark and
            Sweden temporarily halted the use of the vaccine for fear of its link to rare cases of blood clotting
            observed in a small number of vaccine recipients. After the European Medicines Agency's statement, European
            countries resumed vaccinations with AstraZeneca. However, the controversy continued over the vaccine's
            relationship to rare blood clotting cases after doubting the vaccine's data about its effectiveness, which
            was later corrected, in addition to the death of seven people in Britain who received the AstraZeneca
            vaccine.
        </p>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
@endsection