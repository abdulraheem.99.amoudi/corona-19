@extends('layouts.master')
@section('title')
Covid-19 Pfizer
@endsection
@section('content')
<div class="col-12" id="accordion" style="margin-top:20px">
    <div class="card card-primary card-outline">
        <a class="d-block w-100" data-toggle="collapse" href="#collapseOne">
            <div class="card-header">
                <h4 class="card-title w-100">
                    1. What categories should be given priority for vaccination?
                </h4>
            </div>
        </a>
        <div id="collapseOne" class="collapse " data-parent="#accordion">
            <div class="card-body">
                Given the limited supplies of the vaccine, it is recommended that priority be given to health workers at
                high risk of infection and the elderly, including people 65 years of age or older. <br>
                Countries can refer to the FAO Priority Setting Roadmap and FAO Values Framework for guidance in setting
                their priorities for target groups.
            </div>
        </div>
    </div>
    <div class="card card-primary card-outline">
        <a class="d-block w-100" data-toggle="collapse" href="#collapseTwo">
            <div class="card-header">
                <h4 class="card-title w-100">
                    2. What other groups can receive the vaccination?
                </h4>
            </div>
        </a>
        <div id="collapseTwo" class="collapse" data-parent="#accordion">
            <div class="card-body">
                The vaccine has been shown to be safe and effective in people with various medical conditions that are
                associated with an increased risk of severe disease. <br>
                This includes high blood pressure, diabetes, asthma, pulmonary disease, liver and kidney disease, as
                well as stable and controlled chronic conditions. <br>
                More studies need to be done on the effects of the vaccine on immunocompromised people. The initial
                recommendation indicates that immunocompromised persons belonging to a recommended group can be
                vaccinated, but not before providing information and advice on this, when possible. <br>
                People living with HIV are at greater risk of developing severe disease from COVID-19. Clinical trials
                provide limited data on the safety of vaccines in people with HIV who are under good medical
                supervision. Those who receive vaccinations should be informed of the available data and advised in this
                regard, as much as possible. <br>
                The vaccination can be offered to people who have had COVID-19 infection in the past. However, due to
                vaccine supplies being limited, these people may wish to defer vaccination for up to 6 months after they
                have contracted COVID-19. <br>
                No studies have been conducted on the use of the vaccine in lactating women, however, this vaccine does
                not use live viruses, and the messenger RNA does not enter the cell nucleus and is rapidly degraded, and
                therefore cannot interfere with cell functions. <br>
                If a breastfeeding woman belongs to a group that is recommended for vaccination (for example, health
                workers), she can be offered the vaccination. The SAGE does not recommend stopping breastfeeding after
                vaccination. More evidence is being sought to guide the development of WHO's policy recommendations in
                this regard. <br>
            </div>
        </div>
    </div>
    <div class="card card-primary card-outline">
        <a class="d-block w-100" data-toggle="collapse" href="#collapseThree">
            <div class="card-header">
                <h4 class="card-title w-100">
                    3. Should pregnant women be vaccinated?
                </h4>
            </div>
        </a>
        <div id="collapseThree" class="collapse" data-parent="#accordion">
            <div class="card-body">
                While pregnancy increases women's risk of severe disease from COVID-19, there is very little data
                available to assess the safety of vaccines during pregnancy. <br>
                Pregnant women can receive the vaccination if the benefits of vaccinating the pregnant woman outweigh
                the risks. <br>
                For this reason, pregnant women who are at high risk of infection with the Covid-19 virus (such as
                health workers) or who have comorbidities that increase their risk of severe disease can be vaccinated,
                provided that they consult with their health care provider in this regard. <br>
            </div>
        </div>
    </div>
    <div class="card card-warning card-outline">
        <a class="d-block w-100" data-toggle="collapse" href="#collapseFour">
            <div class="card-header">
                <h4 class="card-title w-100">
                    4. What groups should not receive the vaccination?
                </h4>
            </div>
        </a>
        <div id="collapseFour" class="collapse" data-parent="#accordion">
            <div class="card-body">
                The vaccine should not be given to people who have previously had a severe allergic (allergic) reaction
                to any component of the vaccine. <br>
                The vaccine has only been tested on children over 16 years of age. Therefore, WH O does not currently
                recommend vaccinating children under 16 years of age, even if they belong to a high-risk group.
            </div>
        </div>
    </div>
    <div class="card card-warning card-outline">
        <a class="d-block w-100" data-toggle="collapse" href="#collapseFive">
            <div class="card-header">
                <h4 class="card-title w-100">
                    5. What is the recommended dose?
                </h4>
            </div>
        </a>
        <div id="collapseFive" class="collapse" data-parent="#accordion">
            <div class="card-body">
                The protective effect of the vaccine begins 12 days after receiving the first dose, but to gain full
                protection requires receiving two doses of the vaccine, and the organization recommends that the
                interval between the two doses range from 21 to 28 days. More research is needed to understand the
                potential long-term protection of the vaccine after a single dose.
            </div>
        </div>
    </div>
    <div class="card card-warning card-outline">
        <a class="d-block w-100" data-toggle="collapse" href="#collapseSix">
            <div class="card-header">
                <h4 class="card-title w-100">
                    6. Is the vaccine safe?
                </h4>
            </div>
        </a>
        <div id="collapseSix" class="collapse" data-parent="#accordion">
            <div class="card-body">
                WHO authorized the emergency use of the Pfizer-Biontech vaccine under the Emergency Use Protocol on
                December 31, 2020. WHO has conducted a comprehensive evaluation of the quality, safety and efficacy of
                the vaccine and has recommended its use in people over 16 years of age. <br>
                The Global Advisory Committee on Vaccine Safety, a group of experts that provides independent and
                authoritative guidance to WHO on the topic of the safe use of vaccines, receives and evaluates reports
                of suspected safety events that may have a potential international impact.
            </div>
        </div>
    </div>
    <div class="card card-danger card-outline">
        <a class="d-block w-100" data-toggle="collapse" href="#collapseSeven">
            <div class="card-header">
                <h4 class="card-title w-100">
                    7. How effective is the vaccine?
                </h4>
            </div>
        </a>
        <div id="collapseSeven" class="collapse" data-parent="#accordion">
            <div class="card-body">
                The Pfizer-Biontech COVID-19 vaccine is 95% effective against symptomatic COVID-19 infection.
            </div>
        </div>
    </div>
    <div class="card card-danger card-outline">
        <a class="d-block w-100" data-toggle="collapse" href="#collapseEight">
            <div class="card-header">
                <h4 class="card-title w-100">
                    8. Is the vaccine effective against the new mutated strains?
                </h4>
            </div>
        </a>
        <div id="collapseEight" class="collapse" data-parent="#accordion">
            <div class="card-body">
                The Strategic Advisory Group of Experts reviewed all available data on vaccine performance as part of
                tests aimed at evaluating the effectiveness of the vaccine against a variety of mutated strains. These
                tests showed that the vaccine is effective against mutated strains of the virus. <br>
                The Strategic Advisory Group of Experts currently recommends the use of the Pfizer-Biontech vaccine
                according to the WHO roadmap for priority setting, even if mutated strains of the virus are circulating
                in a country. Countries should assess the risks and benefits, taking into account their epidemiological
                situations. <br>
                Preliminary results highlight the urgent need for a coordinated approach to surveillance and evaluation
                of mutated strains and their potential impact on vaccine efficacy. As soon as new data becomes
                available, WHO will update the recommendations accordingly.
            </div>
        </div>
    </div>
    <div class="card card-danger card-outline">
        <a class="d-block w-100" data-toggle="collapse" href="#collapseNine">
            <div class="card-header">
                <h4 class="card-title w-100">
                    9. Does the vaccine protect against infection and its transmission to others?
                </h4>
            </div>
        </a>
        <div id="collapseNine" class="collapse" data-parent="#accordion">
            <div class="card-body">
                There are currently insufficient data available on the effect of the Pfizer-Biontech vaccine on
                transmission or transmission of viruses from the cell. <br>
                In the meantime, we must continue to apply and reinforce effective public health measures: mask wearing,
                physical distancing, hand washing, respiratory hygiene and cough and sneeze etiquette, avoiding crowds,
                and ensuring good ventilation.
            </div>
        </div>
    </div>
    <div class="card card-danger card-outline">
        <a class="d-block w-100" data-toggle="collapse" href="#collapseTen">
            <div class="card-header">
                <h4 class="card-title w-100">
                    10. What about other vaccines being developed to combat COVID-19?
                </h4>
            </div>
        </a>
        <div id="collapseTen" class="collapse" data-parent="#accordion">
            <div class="card-body">
                WHO usually does not issue specific recommendations for vaccines, but rather one that includes all
                vaccines that target a specific disease, unless the evidence calls for a different approach. <br>
                Given the wide range of COVID-19 vaccines based on highly diverse core technologies, WHO is studying
                vaccines whenever they are licensed by highly qualified national regulatory authorities and vaccines
                with sufficient supplies to meet the needs of many countries. <br>
                WHO does not have a preference for a particular product, and considers that the availability of a
                variety of products, including a variety of specific characteristics and handling requirements, allows
                countries to find the products best suited to their own circumstances. <br>
                It is expected that the WHO Strategic Advisory Group of Experts on Immunization will conduct a review of
                other vaccines in the coming months.
            </div>
        </div>
    </div>
</div>
@endsection