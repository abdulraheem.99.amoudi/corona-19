@extends('layouts.master')
@section('title')
Covid-19 Countries
@endsection
@section('content')
<!--
@foreach($countries['Countries'] as $country) 
<li><i class="fa fa-star"></i> {{ print_r($country['Country']) }}</li>
@endforeach
-->
<!-- /.card -->
<!-- general form elements disabled -->
<div class="card card-secondary" style="margin-top:20px">
    <div class="card-header">
        <h3 class="card-title">Countries</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table id="example2" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>Country</th>
                    <th>NewConfirmed</th>
                    <th>TotalConfirmed</th>
                    <th>NewDeaths</th>
                    <th>TotalDeaths</th>
                    <th>NewRecovered</th>
                    <th>TotalRecovered</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody>
                @foreach($countries['Countries'] as $country)
                <tr>
                    <td> @php echo $country['Country'] @endphp</td>
                    <td> @php echo $country['NewConfirmed'] @endphp</td>
                    <td> @php echo $country['TotalConfirmed'] @endphp</td>
                    <td> @php echo $country['NewDeaths'] @endphp</td>
                    <td> @php echo $country['TotalDeaths'] @endphp</td>
                    <td> @php echo $country['NewRecovered'] @endphp</td>
                    <td> @php echo $country['TotalRecovered'] @endphp</td>
                    <td> @php echo $country['Date'] @endphp</td>
                </tr>
                @endforeach

            </tbody>
            <tfoot>
                <tr>
                    <th>Country</th>
                    <th>NewConfirmed</th>
                    <th>TotalConfirmed</th>
                    <th>NewDeaths</th>
                    <th>TotalDeaths</th>
                    <th>NewRecovered</th>
                    <th>TotalRecovered</th>
                    <th>Date</th>
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
@endsection

@section('cssstyle')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('covid-19/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('covid-19/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('covid-19/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
<!-- Theme style -->
<link rel="stylesheet" href="{{ asset('covid-19/dist/css/adminlte.min.css') }}">
@endsection

@section('jscode')
<!-- DataTables  & Plugins -->
<script src="{{ asset('covid-19/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('covid-19/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('covid-19/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('covid-19/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src=".{{ asset('covid-19/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('covid-19/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('covid-19/plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('covid-19/plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('covid-19/plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('covid-19/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('covid-19/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('covid-19/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('covid-19/dist/js/adminlte.min.js') }}"></script>

<script>
$(function() {

    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });
});
</script>
@endsection