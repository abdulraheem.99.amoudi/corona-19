@extends('layouts.master')
@section('title')
Covid-19 Sick
@endsection
@section('content')
<!-- /.card -->
<!-- general form elements disabled -->
<div class="card card-success" style="margin-top:20px">
    <div class="card-header">
        <h3 class="card-title">What to do when feeling sick?</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <p>
        <ul>
            <li>
                <b>Learn about the full spectrum of COVID-19 symptoms.</b> The most common symptoms of COVID-19 are
                fever, dry
                cough and fatigue. Other less common symptoms that may affect some patients include loss of taste or
                smell,
                aches and pains, headache, sore throat, nasal congestion, red eyes, diarrhea, and rash.
            </li>
            <li>
                <b>Stay home and isolate yourself, even if you have mild symptoms such as cough, headache and mild
                    fever,</b>
                until you recover. Call your healthcare provider or hotline for advice. Ask someone else to bring you
                the
                purchases. And if you have to leave the house or summon someone to stay with you, wear a mask to avoid
                spreading the infection to others.
            </li>
            <li>
                <b>If you have a fever, cough and difficulty breathing, seek medical attention immediately. Phone first
                    if
                    you can,</b> and follow directions from your local health authority.
            </li>
        </ul>
        </p>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
@endsection